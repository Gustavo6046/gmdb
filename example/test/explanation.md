# What's going on here, sir?!

In this folder, there is an empty .gmdb folder (which is actually a
mount point, used when, and only when, a GMDB operation is performed),
and a "GMDB image", which is a zip file called '.gmdb-img.zip'. This
image can be viewed using any zip editor, or mounted using fuse-zip.

This folder therefore serves as a sort of container for the image,
which, in turn, is where all the GMDB 'databases' are located.

Try it: do 

```
    ../../scripts/gmdb.list -d testdb
```

from this very folder! Then pick any ID and

```
    ../../scripts/gmdb.cat -d testdb -i "$ID"
```

and amuse yourself! :)