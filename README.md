# Git Multi Database

GMDB is a simple toolkit that allows to create multiple transparent and
completely self-contained document databases within a folder.

They are usually inside an image, which is mounted to a temporary folder
everytime operations must be done in the databases.

## How to use

```bash
    $ mkdir my_container
    $ ./gmdb.init -p my_container
    $ ./gmdb.mkdb -p my_container -d my_database
    $ ./gmdb.mkdoc -p my_container -d my_database -t "My cute document"
      # let's assume the document ID above is saved as $ID
    $ ./gmdb.edit -p my_container -d my_database -i "$ID" -e vim # or micro or nano, you get the idea
      # do your stuff to edit the file! :)
    $ ./gmdb.cat -p my_container -d my_database -i "$ID"
      # file content will appear here
```

## Dependencies

In order for GMDB to work, the following non-builtin commands must also be
available:

```
    fuser-zip
    fusermount
    column
```