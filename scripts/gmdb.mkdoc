#!/bin/bash

#########################################
#    GMDB Document Creating Utility     #
# ------------------------------------- #
# (c)2019 Gustavo Ramos Rehermann.      #
#                                       #
# This script creates a new empty       #
# document in the specified GMDB        #
# database, with an ID and a specific   #
# title.                                #
#########################################
#
# This script, as well as the rest of the
# GMDB codebase, is licensed under the terms
# of the Copyfree Open Innovation License 0.5.
# See LICENSE for more info.
##

DIR="."
DB=""
ID="$(uuidgen)"
TITLE=""

print_help() {
    cat <<EOF
    GMDB Document Creating Utility. (ID-oriented Edition)
        
Usage:
        
    $0 [-h]
    $0 [-p <path>] -d <database> [-i <id>] -t <title>

Arguments:
    -h          Displays this help message.
    database    The name of the database.
    path        The path to the GMDB container.
                Defaults to '.'.
    id          UUID of document to be created.
                Is random by default.
    title       Title of the new document.

----
(c)2019 Gustavo Ramos Rehermann. Also see LICENSE for
source code licensing info.
        
EOF
}

# shellcheck source=.gmdb.common
source "$(dirname "$0")/.gmdb.common"

while getopts 'd:p:i:t:h' option; do
    case "$option" in
        h) print_help; exit 0 ;;
        :) print_help; exit 1 ;;
        p) DIR="$OPTARG" ;;
        d) DB="$OPTARG" ;;
        i) ID="$OPTARG" ;;
        t) TITLE="$OPTARG" ;;
        *) print_help; exit 1 ;;
        
    esac
done
shift $((OPTIND - 1))

# Sanity checks.
if [[ -z "$DB" ]]; then
    >&2 cat <<EOF
Please supply a database name via the -d
argument!

Aborting.
EOF
    exit 1
    
fi

if [[ -z "$TITLE" ]]; then
    >&2 cat <<EOF
Please supply a title for the new document!

Aborting.
EOF
    exit 1

fi

if [[ -z "$ID" ]]; then
    >&2 cat <<EOF
Please supply the ID for the new document!
the -i argument!

Aborting.
EOF

    exit 1

fi

# Mount the gmdb image if not mounted yet.
MOUNTED_NOW=false

if [[ ! -f "$DIR/.gmdb-mounted" ]]; then
    if [[ ! -f "$DIR/.gmdb-img.zip" ]]; then
        >&2 cat <<EOF
There is no GMDB image in this
container! Use gmdb.init to
create one.

Aborting.
EOF
        exit 1
    fi

    if [[ ! -f "$DIR/.gmdb" ]]; then
        mkdir -pv "$DIR/.gmdb"
    fi

    echo Mounting GMDB...
    gmdb-mount "$DIR/.gmdb-img.zip" "$DIR/.gmdb" 2>&1 | awk '{print "| " $0}' || {
        >&2 cat <<EOF
Error mounting the .gmdb-img image to the
.gmdb folder!

Aborting.
EOF
        exit 1
    }

    MOUNTED_NOW=true
fi

# Check whether the database exists.
if [[ ! -f "$DIR/.gmdb/list/$DB" ]]; then
    >&2 cat <<EOF
Database $DB not found!

Aborting.
EOF

    exit 1
    
fi

# Create the document.
if [[ -f "$DIR/.gmdb/repos/$DB/$ID" ]]; then
    {
        echo The document already exists!
        echo
        echo Aborting.
    } >&2

    exit 1
fi

{
    touch  "$DIR/.gmdb/repos/$DB/$ID"
    printf '%s\t%s\t%s\n' "$ID" "$TITLE" "$(date)" >>"$DIR/.gmdb/list/$DB"
} 2>&1 | awk '{ print "| " $0 }'  || {
    >&2 cat <<EOF
Error creating document!

Aborting.
    
EOF

    exit 1
}

# Unmount GMDB.
if [[ "$MOUNTED_NOW" == 'true' ]]; then
    gmdb-unmount "$DIR/.gmdb" || {
        {
            # ahh, prettier. I wish I had thought of this from the beginning.
            echo 'Unknown error unmounting the database!'
            echo
            echo Aborting.
    
            exit 1
        } >&2
    }

fi


echo Document created successfully.
echo ----
{
    printf 'ID:\t%s\n' "$ID"
    printf 'Title:\t%s\n' "$TITLE"
} | column -t -s"$(printf '\t')"

exit 0