#!/bin/bash

#########################################
#     GMDB Database Removal Utility     #
# ------------------------------------- #
# (c)2019 Gustavo Ramos Rehermann.      #
#                                       #
# This script removes an existing GMDB  #
# database.                             #
#########################################
#
# This script, as well as the rest of the
# GMDB codebase, is licensed under the terms
# of the Copyfree Open Innovation License 0.5.
# See LICENSE for more info.
##

DIR="."
DB=""

print_help() {
    cat <<EOF
    GMDB Database Removal Utility.
        
Usage:
        
    $0 [-h]
    $0 [-p <path>] -d <database>

Arguments:
    -h          Displays this help message.
    database    The name of the database to remove.
    path        The path to the GMDB container.
                Defaults to '.'.

----
(c)2019 Gustavo Ramos Rehermann. Also see LICENSE for
source code licensing info.
        
EOF
}

# shellcheck source=.gmdb.common
source "$(dirname "$0")/.gmdb.common"

while getopts 'd:p:h' option; do
    case "$option" in
        h) print_help; exit 0 ;;
        :) print_help; exit 1 ;;
        p) DIR="$OPTARG" ;;
        d) DB="$OPTARG" ;;
        *) print_help; exit 1 ;;
        
    esac
done
shift $((OPTIND - 1))

# Sanity checks.
if [[ -z "$DB" ]]; then
    >&2 cat <<EOF
Please supply a database name via the -d
argument!

Aborting.
EOF
    exit 1
    
fi

# Mount the gmdb image if not mounted yet.
MOUNTED_NOW=false

if [[ ! -f "$DIR/.gmdb-mounted" ]]; then
    if [[ ! -f "$DIR/.gmdb-img.zip" ]]; then
        >&2 cat <<EOF
There is no GMDB image in this
container! Use gmdb.init to
create one.

Aborting.
EOF
        exit 1
    fi

    if [[ ! -f "$DIR/.gmdb" ]]; then
        mkdir -pv "$DIR/.gmdb"
    fi

    echo Mounting GMDB...
    gmdb-mount "$DIR/.gmdb-img.zip" "$DIR/.gmdb" 2>&1 | awk '{print "| " $0}' || {
        >&2 cat <<EOF
Error mounting the .gmdb-img image to the
.gmdb folder!

Aborting.
EOF
        exit 1
    }

    MOUNTED_NOW=true
fi

# Check whether the database exists.
if [[ ! -f "$DIR/.gmdb/list/$DB" ]]; then
    >&2 cat <<EOF
Database $DB not found!

Aborting.
EOF

    exit 1
    
fi

# Delete the database.
rm    "$DIR/.gmdb/list/$DB"
rm -r "$DIR/.gmdb/repos/$DB"

# Unmount GMDB.
if [[ "$MOUNTED_NOW" == "true" ]]; then
    gmdb-unmount "$DIR/.gmdb" >/dev/null || {
        {
            # ahh, prettier. I wish I had thought of this from the beginning.
            echo 'Unknown error unmounting the database!'
            echo
            echo Aborting.
    
            exit 1
        } >&2
    }
    
fi


echo Database deleted successfully.

exit 0